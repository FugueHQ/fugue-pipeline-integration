#!/bin/bash
##########################################
#
# Test phase
# Run make on makefile if present
# Configure fugue client
# Run fugue update/run dry-runs
# Exit if the dry-runs return anything other than 0
#
##########################################
make
tar -xvzf $ARTIFACT.tar.gz
cd $ARTIFACT
fugue init ${FUGUE_CONDUCTOR_AMI}
fugue user set $FUGUE_USERID $FUGUE_USER_SECRET
cat MANIFEST | jq .composition | sed 's,\\\\,/,g' | sed 's/"//g' > COMPOSITION
echo "newline" >> COMPOSITION
cat COMPOSITION
ERRORCOUNT=0
export ERRORCOUNT
if [[ "${BITBUCKET_BRANCH}" =~ ^feature\/.*$ ]]; then
  if fugue status "${BITBUCKET_BRANCH}-${BITBUCKET_REPO_SLUG}" > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update "${BITBUCKET_BRANCH}-${BITBUCKET_REPO_SLUG}" $COMPOSITION --dry-run
    ERRORCOUNT+=$?
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run COMPOSITION -a "${BITBUCKET_BRANCH}-${BITBUCKET_REPO_SLUG}" --dry-run
    ERRORCOUNT+=$?
  fi
elif [[ "${BITBUCKET_BRANCH}" == "develop" ]]; then
  if fugue status develop-${BITBUCKET_REPO_SLUG} > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update develop-${BITBUCKET_REPO_SLUG} $COMPOSITION --dry-run
    ERRORCOUNT+=$?
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run $COMPOSITION -a develop --dry-run
    ERRORCOUNT+=$?
  fi
elif [[ "${BITBUCKET_BRANCH}" =~ ^release\/.*$|^hotfix\/.*$ ]]; then
  if  fugue status staging-${BITBUCKET_REPO_SLUG} > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update staging-${BITBUCKET_REPO_SLUG} $COMPOSITION --dry-run
    ERRORCOUNT+=$?
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run $COMPOSITION -a staging-${BITBUCKET_REPO_SLUG} --dry-run
    ERRORCOUNT+=$?
  fi
elif [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
  if  fugue status production-${BITBUCKET_REPO_SLUG} > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update production-${BITBUCKET_REPO_SLUG} $COMPOSITION --dry-run
    ERRORCOUNT+=$?
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run $COMPOSITION -a production-${BITBUCKET_REPO_SLUG} --dry-run
    ERRORCOUNT+=$?
  fi
else
  exit 1
fi

if [[ "$ERRORCOUNT" -ne "0" ]]; then
  echo "fugue dry-run failed"
  exit 1
fi

##########################################
#
# Deploy phase
#
##########################################

if [[ "${BITBUCKET_BRANCH}" =~ ^feature\/.*$ ]]; then
  if fugue status "${BITBUCKET_BRANCH}-${BITBUCKET_REPO_SLUG}" > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update "${BITBUCKET_BRANCH}-${BITBUCKET_REPO_SLUG}" $COMPOSITION -y
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run COMPOSITION -a "${BITBUCKET_BRANCH}-${BITBUCKET_REPO_SLUG}"
  fi
elif [[ "${BITBUCKET_BRANCH}" == "develop" ]]; then
  if fugue status develop-${BITBUCKET_REPO_SLUG} > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update develop-${BITBUCKET_REPO_SLUG} $COMPOSITION -y
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run $COMPOSITION -a develop-${BITBUCKET_REPO_SLUG}
  fi
elif [[ "${BITBUCKET_BRANCH}" =~ ^release\/.*$|^hotfix\/.*$ ]]; then
  if  fugue status staging-${BITBUCKET_REPO_SLUG} > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update staging-${BITBUCKET_REPO_SLUG} $COMPOSITION -y
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run $COMPOSITION -a staging-${BITBUCKET_REPO_SLUG}
  fi
elif [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
  if  fugue status production-${BITBUCKET_REPO_SLUG} > /dev/null; then
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue update production-${BITBUCKET_REPO_SLUG} $COMPOSITION -y
  else
    COMPOSITION=`head -n 1 ./COMPOSITION`
    fugue run $COMPOSITION -a production-${BITBUCKET_REPO_SLUG}
  fi
else
  exit 1
fi
rm -rf COMPOSITION
exit 0
