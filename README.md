Integration:
============

Fugue and Atlassian Bitbucket Pipelines
---------------------------------------

Overview
========

Bitbucket Pipelines is a continuous delivery service built on top of [Bitbucket
Cloud](https://bitbucket.org). This integration uses
[Fugue](http://www.fugue.co) to build and update infrastructure in your Amazon
Web Services account, following the stages of the Gitflow model, by pushing
Fugue code to corresponding branches in Bitbucket.

Prerequisites
-------------

-   [Create an AWS IAM
    user](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html)
    with an `AdministratorAccess` policy applied (see "Setup" below).

-   Ensure your AWS credentials are available to the command line, either via IAM role or a credentials file (~/.aws/credentials), typically created by installing and configuring the AWS CLI. You can create this file manually as well, with the access key and secret key from the user we created. It follows this format

        [default]
        aws_access_key_id=AKIATHISISMYKEY
        aws_secret_access_key=SomeReallyLongKeyValue    

-   Obtain the Fugue Client Tools [here](https://fugue.co/users/register/).

    *You will need to know your AWS account id. If you're an IAM user without access to the account information, there are many ways to get this, as it a component of any ARN created in the account. One simple way is with the AWS CLI as the administrator use you just created and type this:*
    
    `aws sts get-caller-identity --output text --query 'Account'`

-   **Note: For the purpose of this tutorial, the current artifacts were
    downloaded and re-hosted in an S3 bucket. You may need to do similar with
    your artifacts and bucket. You will need to update the
    bitbucket-pipelines.yml file accordingly.**

-   Obtain a Bitbucket Cloud account
    [here](https://bitbucket.org/product/features/pipelines).
    
    

Usage
=====

[Enable Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html)
on the Bitbucket repository, using the “Other” template. The Pipeline
configuration is defined in bitbucket-pipelines.yml. Please refer to the
Bitbucket Pipelines documentation for details. Pipeline builds are triggered on
pushes to the Bitbucket repo branches, when accompanied by an [lwc](https://docs.fugue.co/ludwig-guide-lwc.html) snapshot. After pushing a change to a particular
branch, the build will begin executing the build steps as defined in
bitbucket-pipelines.yml. The Fugue client will be installed as part of the build
process. Fugue compile-time validations and dry-run checks will be executed in a
test phase before execution. Assuming tests pass, the deployment phase will
initiate a `fugue run` to launch the composition as a process, or execute a `fugue
update` if a process with the Fugue alias already exists. The alias is
dynamically named based on the **BITBUCKET\_BRANCH** environment variable whose
value is the git branch currently building and the **BITBUCKET\_REPO\_SLUG** whose
value is the git repo URL slug. We use `fugue status` to check if an alias exists
for a given deployment stage, and if it does, we issue a `fugue update` instead of
a `fugue run`.



The deployment stages are modeled after the
[Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
branching model.

| Environment | Branch Name   | Fugue Alias                           |
|-------------|---------------|---------------------------------------|
| feature     | feature regex | dynamic based on branch name and repo |
| develop     | develop       | develop-{repo\_slug}                  |
| staging     | release regex | staging-{repo\_slug}                  |
| production  | master        | production-{repo\_slug}               |




Setup
=====

Create an IAM user with `AdministratorAccess` rights and store the access key and secret key for it

![ci](media/ad5bbe260543a74f397f716b5e2ffd9b.png)


Install the Fugue Client Tools. Refer to the Fugue [documentation](https://docs.fugue.co/fugue-by-example-hello-world-1.html#fugue-quick-setup) for details.

Once installed, we'll need to collect the version number for later. 
- - -

On a Mac, use the following command to find the version number:

`brew cask info fugue-client-release | grep (pkg)`

~~~~
$ brew cask info fugue-client-release
fugue-client-rc: 0.27.11-1728
https://fugue.co/
Not installed
From: git@github.com:LuminalHQ/homebrew-fugue.git
==> Name
Fugue Client
==> Artifacts
fugue-client-0.27.11-1728.pkg (pkg)
~~~~
_If you did not install the client via brew, the version number is actually part of the file name_
_fugue-client-*{version number}*.pkg_


- - -


On Ubuntu 14.04 or 16.04 LTS use: 

`dpkg -s fugue-client | grep Version`

```
$ dpkg -s fugue-client | grep Version
Version: 0.27.11-1728
```
- - -


And finally, on RHEL6 use: 

`$ yum info fugue-client | grep -i version`

```
Version    : 0.27.11-1728
```

- - -

_Store this full numeric version string in a safe place, we'll need it later for an environment variable._





Initialize the project directory
Refer to the Fugue [documentation](https://docs.fugue.co/fugue-by-example-hello-world-1.html#initialize-a-project)
and ensure you have the correct AMI ID from the [Download
Portal](https://download.fugue.co/). Take note of this AMI ID for later.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$ fugue init ami-0db2781b
[ fugue init ] Initializing Fugue project with the following configuration:

Fugue Conductor AMI ID: ami-0db2781b
AWS Credentials: Environment variables

Validating Fugue Conductor AMI ID ...
[ OK ] Provided AMI ID is valid.

Creating new fugue.yaml file ...

[ Done ] Project initialized.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

![ci](media/ff175d41cc2cf059a62a4bd02c7cf305.gif)


Launch the conductor.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$ fugue install
[ fugue install ] Installing Fugue Conductor

Install Details:

   Conductor AMI ID: ami-0db2781b
   AWS Account: fugue-ci-demo/xxxxxxxxxxxx

[ WARN ] Would you like to proceed with installing? [y/N]: y
Installing the Fugue Conductor into AWS account fugue-ci-demo/xxxxxxxxxxxx.

FugueVpc                             Complete
FugueSubnet2RouteTableAssociation    Complete
FugueHealthCheckDb                   Complete
FugueInstanceProfile                 Complete
FugueRouteTable                      Complete
FugueLaunchConfiguration             Complete
FugueSubnet1RouteTableAssociation    Complete
FugueVpcGateway                      Complete
FugueInternetRoute                   Complete
FugueSubnet2                         Complete
FugueSubnet1                         Complete
FugueIam                             Complete
FugueAutoScalingGroup                Complete
FugueResourceEventsTopic             Complete
FugueVpcSecurityGroup                Complete
FugueVpcGatewayAttachment            Complete
-----------------------------------------------
Overall Progress  [#########################]  100%

[ HELP ] Exiting the install command while in progress (CTRL+C) will only stop progress tracking and *not* the install itself.

[ OK ] Fugue Conductor installed.

Booting the Conductor, please wait as this may take between 5-15 minutes...

[ HELP ] The Conductor needs to boot before it can accept commands from the CLI. Exiting the install command while in progress (CTRL+C) will only stop progress tracking and *not* the install itself or the booting process.

[ DONE ] Fugue has been successfully installed and is ready to receive commands.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

![ci](media/d0bacdfd3ee021ab57b3ad0b017da334.gif)

- - -


__Set up Bitbucket Pipelines__

You’ll need to do the following steps:

-   Create the repo

-   Initialize the repo with a file, such as a README

-   Enable Pipelines

-   Configure the build process (select the "Other" template and paste in the contents of bitbucket-pipelines.yml)

See the GIF below for more details.

![ci](media/pipelinecrop.gif)

Next, you’ll need to download the Fugue Client Tools from the [Fugue Download Portal](https://download.fugue.co) again, but this time, download the Ubuntu version. If your local client OS is Ubuntu, you can skip this step and reuse the file. This is the client that will be installed as part of the Pipelines build process. You have a chance to copy the AMI id again if you forgot to before.

![ci](media/downloadcrop.gif)

- - -

Finally, you’ll need to add the environment variables to the repo that your
builds will depend on. 

**AWS\_ACCESS\_KEY\_ID**: This is the access key for the IAM user we created at the start

**AWS\_SECRET\_ACCESS\_KEY**: This is the secret key for the IAM user we created at the start

**FUGUE\_CLIENT\_VERSION**: The version id of the Fugue client that we got from our local install

**FUGUE\_CONDUCTOR\_AMI**: The AMI id we copied from the Fugue download portal

**FUGUE\_USERID**: This is the userid of the user the client will run as on your
build host. If you are using the Basic Conductor, this user is `root`.

**FUGUE\_USER\_SECRET**: This is the secret to go with the userid above.

**ARTIFACT**: This is the file handle for the snapshot you include with your commit
when you want to trigger a build. By default, we use `snapshot`. Changes to this
value may necessitate changes to build.sh.

You can get **FUGUE\_USERID** and **FUGUE\_USER\_SECRET** by running this in the directory
with your [fugue.yaml
file](https://docs.fugue.co/fugue-by-example-hello-world-1.html#fugue-quick-setup):

```
cat ./fugue.yaml | grep -i user
```



Once collected, add them under “Settings” \> “Environment variables” in your
repo menus.

![ci](media/envvars.PNG)

- - -

Running The Composition
=======================

If you’ve set everything up and are regularly committing your Ludwig to the
repo, you may notice your builds always fail. This is by design. To trigger an
infrastructure action you need to add a snapshot to the commit. For more reading
on snapshots, go [here](https://docs.fugue.co/ludwig-guide-lwc.html). When
everything compiles locally, build a snapshot like this:



```lwc MyComposition.lw -s snapshot -o snapshot.tar.gz```



_**Remember! Whatever name you put before the ‘tar.gz’ extension needs to match the
value for the ARTIFACT environment variable we set above.**_

Then add the snapshot to the commit, and push it to the repo.

This file being present will trigger a build of your composition into
infrastructure.

The output of the pipeline build will show you the alias of the Fugue process
created so you can continue to monitor it from the Fugue client.

Other sections of the build log will show the results of a `fugue run`/`update`
`--dry-run` of your infrastructure so you can verify what changes will happen as
part of the run.

![./media/success.PNG](media/success.PNG)

- - -

_**Be sure to check out a visualization of your infrastructure code at <https://playground.fugue.co>**_

[![Fugue Visualization](media/viz.PNG)](https://playground.fugue.co)


_**For more information and updates from Fugue, check out <https://www.fugue.co>**_